#Tib Seneut -> Bit Tunes
Concept album composed using:

-   Various Programming Languages
    +   Chuck
    +   SuperCollider
-   Software
    + Soundtrap
-   Random Recordings
    + Friends
    + Lover
-   Ambient Ideology
-   Minimalist values
-   Frustrations