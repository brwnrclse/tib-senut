/*
    Track One : layer 3
    Step up
 */

// Step to DAC
Step s => dac;

// Float value for step magnitude
.5 => float v;

while (true) {
    200::ms => now;
    v => s.next;
    -v => v;
}
