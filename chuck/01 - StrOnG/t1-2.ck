/*
    Track One : layer 2
    Quick impulse on the 1/4 of layer 1
 */

// ChucK impulse into filter into DAC
Impulse i => BiQuad f => dac;

// Set filter pole radius
.99 => f.prad;

// Set eqaual gains
1 => f.eqzs;

// Float to be used for filter frequency
1.0 => float v;

while (true) {
    // Timing
    100::ms => now;
    // Set current impulse
    1.0 => i.next;
    // Exponentially increase float
    v * 4000.0 => f.pfreq;
}
