/*
Track one : layer one
    simple sine oscillator, few backing beets. (Yes food)
 */


// chuck sine oscialltor(s) into DAC
SinOsc low => dac;
SinOsc mid => dac;

while (true) {
    1::second => now;
    // ChucK a random frequency into the osciallator(s) every second...forever
    Std.rand2f(30.0, 55.0) => low.freq;
    Std.rand2f(80.0, 105.0) => mid.freq;
}
